from django.apps import AppConfig


class MiaAppConfig(AppConfig):
    name = 'mia_app'
