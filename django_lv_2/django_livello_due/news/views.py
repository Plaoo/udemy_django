from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse


from .models import Articolo, Giornalista

def home(request):
    articoli = Articolo.objects.all()
    giornalisti = Giornalista.objects.all()
    context = {'articoli':articoli,
            'giornalisti':giornalisti,}

    return render(request, "homepage.html", context)

def articoloDetailView(request, pk): 
    # articolo = Articolo.objects.get(pk=pk)
    articolo = get_object_or_404(Articolo, pk = pk)
    context = {'articolo': articolo}
    return render(request, "articolo_detail.html", context)

### GCBV Generic Class Base View
#Documentazione https://docs.djangoproject.com/en/2.1/topics/class-based-views/

from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

class ArticoloDetailViewCB(DetailView):
    model = Articolo
    template_name= "articolo_detail.html"

class ArticoloListView(ListView):
    model = Articolo
    template_name = "lista_articoli.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["articoli"] = Articolo.objects.all()
        return context
