from django.urls import path

from .views import home, articoloDetailView, ArticoloDetailViewCB, ArticoloListView

urlpatterns = [
    path('', home, name="homeview"),
    
    #path("articolo/<int:pk>", articoloDetailView, name="articolo_detail")
    path("articolo/<int:pk>", ArticoloDetailViewCB.as_view(), name="articolo_detail"),
    path("lista_articoli/", ArticoloListView.as_view(), name="lista_articoli")

]
