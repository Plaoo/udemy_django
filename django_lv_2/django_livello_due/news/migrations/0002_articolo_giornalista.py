# Generated by Django 2.0.5 on 2018-07-31 19:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='articolo',
            name='giornalista',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='news.Giornalista'),
            preserve_default=False,
        ),
    ]
