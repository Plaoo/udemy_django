# Generated by Django 2.0.7 on 2018-08-12 10:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_articolo_giornalista'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='articolo',
            options={'verbose_name': 'Articolo', 'verbose_name_plural': 'Articoli'},
        ),
        migrations.AlterModelOptions(
            name='giornalista',
            options={'verbose_name': 'Giornalista', 'verbose_name_plural': 'Giornalisti'},
        ),
        migrations.AlterField(
            model_name='articolo',
            name='giornalista',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='articoli', to='news.Giornalista'),
        ),
        migrations.AlterField(
            model_name='giornalista',
            name='cognome',
            field=models.CharField(max_length=40),
        ),
        migrations.AlterField(
            model_name='giornalista',
            name='nome',
            field=models.CharField(max_length=40),
        ),
    ]
