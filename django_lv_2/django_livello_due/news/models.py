from django.db import models
from django.urls import reverse

class Giornalista(models.Model):
    nome = models.CharField(max_length = 40)
    cognome = models.CharField(max_length = 40)

    def __str__(self):
        return self.nome + " " + self.cognome

    class Meta:
        '''Questa classe serve per la visualizzazione dei nomi
        corretti nel pannello admin'''

        verbose_name = "Giornalista"
        verbose_name_plural = "Giornalisti"

class Articolo(models.Model):
    """ Il modello generico di un articolo di news """
    titolo = models.CharField(max_length=100) #lunghezza e' obbligatoria
    contenuto = models.TextField()
    giornalista = models.ForeignKey(Giornalista, on_delete=models.CASCADE, related_name="articoli")

    def __str__(self):
        return self.titolo

    def get_absolute_url(self):
        return reverse("articolo_detail", kwargs={"pk": self.pk})

    class Meta:
        verbose_name = "Articolo"
        verbose_name_plural = "Articoli"


