from django.db import models
from django.urls import reverse

class Autore(models.Model):
    """Modello per gli autori"""

    nome = models.CharField(max_length = 40)
    cognome = models.CharField(max_length = 40)
    nazione = models.CharField(max_length = 40)

    def __str__(self):
        return self.nome + " " + self.cognome

    #creazione link dinamico
    def get_absolute_url(self):
        return reverse("autore_detail", kwargs={"pk": self.pk})

    class Meta:
        verbose_name = "Autore"
        verbose_name_plural = "Autori"


class Genere(models.Model):
    """Modello per i generi"""

    nome = models.CharField(max_length = 40)

    def __str__(self):
        return self.nome
    
    class Meta:
        verbose_name = "Genere"
        verbose_name_plural = "Generi"

class Libro(models.Model):
    """Modello per i libri"""

    titolo = models.CharField(max_length = 60)
    autore = models.ForeignKey(Autore, on_delete = models.CASCADE, related_name="libri")
    genere = models.ManyToManyField(Genere)
    isbn = models.IntegerField()

    def __str__(self):
        return self.titolo
    class Meta:
        verbose_name = "Libro"
        verbose_name_plural = "Libri"
