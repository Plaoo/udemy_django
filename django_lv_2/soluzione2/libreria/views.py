from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Autore, Genere, Libro
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

# def home(DetailView):
#     autori = Autore.objects.all()
#     generi = Genere.objects.all()
#     libri = Libro.objects.all()
    
#     context = {
#         'autori': autori,
#         'generi': generi,
#         'libri':libri,
#         }
#     return render(request, "homepage.html", context)

class Home(ListView):
    model = Libro
    template_name = 'homepage.html'

class AutoreDetailsView(DetailView):
    model = Autore
    template_name = 'autore_detail.html'