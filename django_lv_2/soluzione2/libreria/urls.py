from django.urls import path

from .views import Home, AutoreDetailsView

urlpatterns = [
    path('', Home.as_view(), name='homepage'),
    path('autore/<int:pk>/', AutoreDetailsView.as_view(), name="autore_detail"),
]