from django import forms
from django.core.exceptions import ValidationError

class FormContatto(forms.Form):
    nome = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    cognome = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    email = forms.EmailField(widget=forms.TextInput(attrs={"class":"form-control"}))
    contenuto = forms.CharField(widget = forms.Textarea(attrs = {
                                                        "placeholder": "Area di testo",
                                                        "class":"form-control"
                                                        }
                                                        ))  
    def clean_contenuto(self):
        dati = self.cleaned_data["contenuto"]
        if "parola" in dati:
            raise ValidationError("contenuto non valido, parola non accettata")
