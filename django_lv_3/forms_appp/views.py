from django.shortcuts import render
from .forms import FormContatto
from django.http import HttpResponse

# Create your views here.
def homepage(request):
    return render(request, "forms_appp/home.html")

# def contatti(request):
#     form = FormContatto()

#     context = {"form": form}
#     return render(request, "forms_appp/contatto.html", context)

def contatti(request):
    if request.method == "POST":
        form = FormContatto(request.POST)
        if form.is_valid():
            print("il form e' valido")
            print("Nome: ", form.cleaned_data["nome"])
            print("Cognome: ", form.cleaned_data["cognome"])
            print("Email: ", form.cleaned_data["email"])
            print("Contenuto: ", form.cleaned_data["contenuto"])
            print(form.cleaned_data)
            return HttpResponse("<h1> Grazie per averci contattato</h1>")

    else:
        form = FormContatto()
 
    context = {"form": form}
    return render(request, "forms_appp/contatto.html", context)