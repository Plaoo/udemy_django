from django.db import models

# Create your models here.
class BlogPostModel(models.Model):
    titolo = models.CharField(max_length=100)
    contenuro = models.TextField()
    bozza = models.BooleanField()
    