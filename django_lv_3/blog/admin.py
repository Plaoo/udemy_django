from django.contrib import admin

# Register your models here.
from .models import BlogPostModel

admin.site.register(BlogPostModel)